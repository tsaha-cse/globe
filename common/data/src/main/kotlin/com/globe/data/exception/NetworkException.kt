package com.globe.data.exception

object NetworkException : RuntimeException("Network connection is unavailable")